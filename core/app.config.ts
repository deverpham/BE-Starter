const path = require('path');
class Config {
  env: string = 'prod';
  socketEmulatorPoint: string = 'https://zohosub-deverpham.c9users.io';
  localHost: string = 'http://localhost:4002';
  jwtSecret: string = 'esignature-deverpham-carryteam';
  webdomain: string = 'http://localhost:3000';
  adminEmailAddress: string = 'deverpham@gmail.com';
  forgotPasswordTimeExpired: any = 3; // 3 days
}

class EmailConfig {
  transport: {
    service: string;
    auth: {
      user: string;
      pass: string;
    }
  } = {
    service: 'zoho',
    auth: {
      user: 'mail@blueskychat.com',
      pass: 'Blue817**!!$$'
    }
  };
  commonOptions: {
    from: string;
    to: string;
    subject: string;
    html: string;
  } = {
    from: '"Jeremy Raymond" <mail@blueskychat.com>', // Sender address
    to: '', // Recepient address
    subject: 'Hello ✔', // Subject line
    html: '', // Content
  };
}

class LiveChatIncConfig {
  user: string = 'mgroupjsteam@gmail.com';
  apiKey: string = '2fc692b6f3a47757167f4cab148d121d';
  emailDefaultForCreateGroup: string = 'mgroupjsteam@gmail.com';

}

class ZohoSubscriptionConfig {
  endpoint: string = 'https://subscriptions.zoho.com/api/v1';
  organizationId: string = '646606545';
  authenToken: string = 'dd22f00664fd782f025005e707a09f4f';
}
class AppVariables {
  ROOT_PATH = path.join(__dirname, '../../');
  CORE_PATH = path.join(this.ROOT_PATH, './core');
  SERVICE_PATH = path.join(this.CORE_PATH, './service');
  ROUTER_PATH = path.join(this.ROOT_PATH, './router');
  SERVER_PATH = path.join(this.ROOT_PATH, './server');
  TEMPLATE_PATH = path.join(this.ROOT_PATH, './template');
  DOCUMENT_PATH = path.join(this.ROOT_PATH, './userdocument');
}
export let appVariables = new AppVariables();
export let appConfig = new Config();
export let emailConfig = new EmailConfig();
export let liveChatIncConfig = new LiveChatIncConfig();
export let zohoSubscriptionConfig = new ZohoSubscriptionConfig();
