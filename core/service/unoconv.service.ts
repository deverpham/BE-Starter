const fs = require('fs');
const path = require('path');
const request = require('request');
const easyimg = require('easyimage');
const exec = require('sync-exec');
class UnoconvService {
  private urlApi = 'http://localhost:5555/unoconv';
  private _initFormRequest(pathFile) {
    return {
          file : fs.createReadStream(pathFile)
        }
  }
  private _convertAPI(form, format: string) {
    return request.post({
      url: `${this.urlApi}/${format}`,
      formData: form
    })
  }
  convertPDF(sourcePATH, outPATH) {
    return new Promise((resolve, reject) => {
      try {
      const form = this._initFormRequest(sourcePATH);
      const convertProgress = this._convertAPI(form, 'pdf');
      convertProgress
        .pipe(fs.createWriteStream(outPATH))
        .on('close', () => {
          resolve()
        })
      } catch (ex) {
        reject(ex)
      }
    })
  }

  convertJPG(sourcePATH, outPATH) {
    exec(`convert ${sourcePATH} -bordercolor white -border 1x1 -trim -resize 50% ${outPATH}`)
    return;
  }
}

export let convertService = new UnoconvService();
