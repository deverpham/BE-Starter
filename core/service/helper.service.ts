const uniqueString = require('unique-string');
const express = require('express');
const _ = require('underscore');
const sha256 = require('sha256');
class HelperService {
  newRouter() {
    return express.Router();
  }

  response(status: string, value: any) {
    return {
      status,
      value
    };
  }

  generateId(): string {
    return uniqueString();
  }

  checkNull(variable: any) {
    return variable === undefined;
  }

  log(name: string, message: string) {
    console.log(`${name.toUpperCase()} : ${message}`);
  }

  resRejectNull(listKey: any, req: any, res: any) {

    return new Promise((resolve: any, reject: any) => {
      listKey.forEach((key: any, i: number) => {
        this.log(`checking params ${key} is not valid;`, this.checkNull(req.body[key]).toString());
        if (this.checkNull(req.body[key]) || req.body[key] === '') {
          res.status(401).send(this.response('error', `Missing params ${key} or empty value`));
          resolve(true);
        }
        if (i === listKey.length - 1) resolve(false);
      });
    });
  }

  omit(ObjectSource: any, arrayKey: any) { // Dùng Để lọc những cái không cần thiết
    arrayKey.push(
      ['__data', '__strict', '__persisted', '__cachedRelations', 'passwordResetToken', 'passwordResetExpires']);
    return _.omit(ObjectSource, arrayKey);
  }
  without(arr, element) {
    return _.without(arr, element);
  }

  findWhere(arr, element) {
    return _.findWhere(arr, element);
  }
  pick(ObjectSource: any, arrayKey: any) { // Dùng Để chọn những cái  cần thiết

    return _.pick(ObjectSource, arrayKey);
  }
  isString(value: any) { // Kiểm tra có phải là string không
    return _.isString(value);
  }

  isEmpty(object: any) {
    return _.isEmpty(object);
  }

  isArray(value: any) {
    return _.isArray(value);
  }

  isObject(value: any) {
    return _.isObject(value);
  }

  isDate(date: any) {
    return _.isDate(date);
  }

  success(res, data: any) {
    return res.send(data);
  }

  error(res, err: Response) {
    if (err.code === undefined)
    return res.status(400).send(err);
    return res.status(err.code).send(err.message)
  }
  encrypt(message) {
    return sha256(message)
  }
}
class Response {
  code: number;
  message: any;
}
export let helper = new HelperService();
