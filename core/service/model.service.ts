import {app} from '../../server';
const query = require('loopback-filters');
export class ModelService {
  currentModel ;
  protected connectModel(modelName) {
    return new Promise((resolve: any) => {
      const pointer = setInterval(() => {
        if (app.models !== undefined) {
          clearInterval(pointer);
          resolve(app.models[modelName]);
        }
      }, 100);
    });
  }
  add(data: any) {
    return new Promise((resolve: any, reject: any) => {
      try {
        this.currentModel.create(data, (err: any, object: any) => {
          if (err) reject(err.toString());
          resolve(object);
        });
      } catch (err) {
        reject(err.toString());
      }
    });
  }

  delete(id: string) {
    return new Promise((resolve: any, reject: any) => {
      try {
        this.currentModel.destroyById(id, (err: any, object: any) => {
          if (err) reject(err.toString());
          resolve(object);
        });
      } catch (err) {
        reject(err.toString());
      }
    });
  }

  change(id: string, dataChange: any) {
    return new Promise((resolve: any, reject: any) => {
      try {
        this.currentModel.upsertWithWhere({ id }, dataChange, (err: any, object: any) => {
          if (err) reject(err.toString());
          resolve(object);
        });
      } catch (err) {
        reject(err.toString());
      }
    });
  }

  findByObject(queryObject: any = {}) {
    return new Promise((resolve: any, reject: any) => {
      try {
        this.currentModel.find(queryObject, (err: any, object: any) => {
          if (err) reject(err.toString());
          resolve(object);
        });
      } catch (err) {
        reject(err.toString());
      }
    });
  }

  findbyID(id) {
    return new Promise((resolve: any, reject: any) => {
      try {
        this.currentModel.findById(id, (err: any, object: any) => {
          if (err) reject(err.toString());
          resolve(object);
        });
      } catch (err) {
        reject(err.toString());
      }
    });
  }
}

class LoopBackQuery {
  where(sourceData, queryObject) {
    return query(sourceData, {
      where : queryObject
    })
  }
}
export let loopBackQuery = new LoopBackQuery();
