const hummus = require('hummus');
const base64Img = require('base64-img');
import {convertService} from './unoconv.service';
import {appVariables} from '../app.config';
const fs = require('fs');
class PdfService {
  addSignature(SOURCE_PATH, image64, position, page= 0 ) {
    console.log(position);
    return new Promise((resovle, reject) => {
      const pdfWriter = hummus.createWriterToModify(SOURCE_PATH, {
        modifiedFilePath: SOURCE_PATH
      })
      const pageWriter = new hummus.PDFPageModifier(pdfWriter, page, true);
      const PAGE_PDF_INFO = pdfWriter.getModifiedFileParser().parsePage(page).getMediaBox();
      const pageTop = PAGE_PDF_INFO[3];
      const pageRight = PAGE_PDF_INFO[4];
      const imageHeight = pdfWriter.getImageDimensions(image64).height;
      const transform = {
        transformation: {
          width: 70,
          height: 50,
          proportional: true,
          fit: 'always'
        },
      };
      pageWriter
        .startContext()
        .getContext()
        .drawImage(pageTop - position.y, position.x, image64, transform);
      pageWriter.endContext().writePage();
      pdfWriter.end();
      resovle();
    })
  }

  image64toImg(data) {
     const signaturePATH = base64Img.imgSync(data, appVariables.DOCUMENT_PATH, 'signature');
     convertService.convertJPG(signaturePATH, appVariables.DOCUMENT_PATH + `/signature.jpg` )
     return appVariables.DOCUMENT_PATH + `/signature.jpg`;
  }
}
export let pdfService = new PdfService();
