
const nodemailer = require('nodemailer')
const sesTransport = require('nodemailer-ses-transport')
const  juice = require('juice')
const pug = require('pug')
const  path = require('path')
import {appVariables} from '../app.config';
const templates = {
  signatureRequest: pug.compileFile(appVariables.TEMPLATE_PATH + '/email/signature-request.pug'),
  forgotPassword: pug.compileFile(appVariables.TEMPLATE_PATH + '/email/forgotpassword.pug')
}

const transport = nodemailer.createTransport(sesTransport({
    accessKeyId: 'AKIAJFU6A6XKEJU2NZIA',
    secretAccessKey: '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',
    rateLimit: 10,
    region: 'us-west-2'
}))


class EmailService {
  sendMail(type: string, options: EmailObject) {
    const atTime = new Date();
    let subject;
    let templateName;
    switch (type) {
      case 'SIGNATURE_REQUEST':
        subject = `Hello ${options.toName}, you received an signature request from ${options.fromName}`
        templateName = 'signatureRequest'
        break;
      case 'RESET_PASSWORD' :
        subject = `Your password reset at ${atTime.toUTCString().split('G')[0]}`
        templateName = 'forgotPassword'
        break;
      default:
        throw new Error('Unknown mail type')
    }
    return transport.sendMail({
        to: options.to,
        from: 'support@lianbihua.com',
        fromName: 'Support Lianbihua',
        subject,
        html: juice(templates[templateName](options.data))
      })
  }
  forgotEmail(object: EmailObject) {
    this.sendMail('RESET_PASSWORD', object)
  }

  sendSignatureRequestMail(signer, DataOption) {
    console.log("data"  + DataOption);
    this.sendMail('SIGNATURE_REQUEST', {
      to: signer.email,
      toName: signer.name,
      data : {
        title: DataOption.title,
        message: DataOption.message,
        docID: DataOption.id,
        url : DataOption.url,
        email: DataOption.requestEmail,
        uid: DataOption.uid
      }
    })
  }
}
class EmailObject {
  toName: string;
  fromName?: string;
  to: string;
  data?: object;
}
export let emailService = new EmailService();
