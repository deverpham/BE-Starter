import {liveChatIncConfig} from '../app.config';
import {helper} from './helper.service';
const LiveChatApi = require('livechatapi').LiveChatApi;
const api = new LiveChatApi(liveChatIncConfig.user, liveChatIncConfig.apiKey);

const STANDART_TAGS = ['Lead', 'General Enquiry'];
const STANDART_TAGS_UPPERCASE = ['LEAD', 'GENERAL ENQUIRY'];
const NO_CHARGE_TAG = 'No Charge';

class LiveChatIncService {
  createGroup(groupName: string, callback: any) {

    api.groups.create({
      name: groupName,
      agents: [liveChatIncConfig.emailDefaultForCreateGroup]
    }, callback);
  }

  listGroups(): any {
    return new Promise((resolve: any, reject: any) => {
      api.groups.list({}, (data: any) => {
        resolve(data);
      });
    });
  }

  listChats(data: any): any {
    return new Promise((resolve: any, reject: any) => {
      api.chats.list({
        date_from: data.date_from,
        date_to: data.date_to,
        group: data.groupId,
      }, (data: any) => {
        resolve(data.chats);
      });
    });
  }

  listChatsByAdminOrClient(filterObject: any) {
    return new Promise((resolve: any) => {
      api.chats.list(filterObject, (response: any) => {

        response.chats.map((chat: any) => {
          if (chat.tags.length === 0) {
            chat.tags = [NO_CHARGE_TAG];
          } else {
            const tags = chat.tags.map((tag: string) => {
              const index = STANDART_TAGS_UPPERCASE.indexOf(tag.toUpperCase());

              if (index >= 0) tag = STANDART_TAGS[index];
              tag = NO_CHARGE_TAG;
            });
          }
          return chat;
        });
        resolve(response);
      });
    });
  }

  countChats(filterObject: any) {

    return Promise.all([
      new Promise((resolve: any) => {
        // Get total
        api.chats.list(filterObject, (response: any) => {
          resolve(response.total);
        });
      }),
      new Promise((resolve: any) => {
        filterObject.tag = [];
        filterObject.tag.push(STANDART_TAGS[0]);
        api.chats.list(filterObject, (response: any) => {
          // Get lead
          resolve(response.total);
        });
      }),
      new Promise((resolve: any) => {
        filterObject.tag = [];
        filterObject.tag.push(STANDART_TAGS[1]);
        api.chats.list(filterObject, (response: any) => {
          // Get General Enquiry
          resolve(response.total);
        });
      })
    ]);
  }

  getChatsDetailById(chatId: string) {
    return new Promise((resolve: any, reject: any) => {
      api.chats.get(chatId, (data: any) => {
        if (data.error) {
          reject(data.error);
        }
        resolve(data);
      });
    });
  }
  searchChats(queryObject: any): any {
    return new Promise((resolve: any, reject: any) => {
      api.chats.list(queryObject, (data: any) => {
        if (data.error) {
          reject(data.error);
        }
        resolve(data);
      });
    });
  }

  filterReports(req: any): any {
    return new Promise((resolve: any, reject: any) => {
      const filterObject: any = {
        date_from: req.params.dateFrom,
        date_to: req.params.dateTo
      };

      if (req.params.groupId) {
        filterObject.group = req.params.groupId;
      }

      api.chats.list(filterObject, (data: any) => {
        if (data.error) {
          reject(data.error);
        }
        resolve(data.chats);
      });
    });
  }
}

export let liveChatIncService = new LiveChatIncService();
