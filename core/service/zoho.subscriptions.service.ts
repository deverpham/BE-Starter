const axios = require('axios');
const request = require('request');

import {zohoSubscriptionConfig} from '../app.config';
import {helper} from './helper.service';

class ZohoSubscriptionsService {

  updateCreditCard(data: any, customerId: string, cardId: string): any {
    return new Promise((resolve: any, reject: any) => {
      const url = `${zohoSubscriptionConfig.endpoint}/customers/${customerId}/cards/${cardId}`;
      const { card_number, cvv_number, expiry_month, expiry_year, payment_gateway,
        first_name, last_name, street, city, state, zip, country } = data;

      axios.put(url, {
        card_number, cvv_number, expiry_month, expiry_year, payment_gateway,
        first_name, last_name, street, city, state, zip, country
      }, {
          headers: {
            'Content-Type': 'application/json;charset=UTF-8',
            'X-com-zoho-subscriptions-organizationid': `${zohoSubscriptionConfig.organizationId}`,
            'Authorization': `Zoho-authtoken ${zohoSubscriptionConfig.authenToken}`
          }
        }).then((response: any) => {
          resolve(response);
        })
        .catch((error: any) => {
          reject(error.message);
        });
    });
  }

  updateCustomer(user: any): any {
    return new Promise((resolve: any, reject: any) => {
      const url = `${zohoSubscriptionConfig.endpoint}/customers/${user.customer_id}`;
      const body = {
        display_name: user.display_name,
        email: user.email,
        first_name: user.first_name,
        last_name: user.last_name,
        company_name: user.companyname,
        phone: user.phone,
        mobile: user.mobile,
        billing_address: {
          street: user.street,
          city: user.city,
          state: user.state,
          zip: user.zip,
          country: user.country
        }
      };

      axios.put(url, body, {
          headers: {
            'Content-Type': 'application/json;charset=UTF-8',
            'X-com-zoho-subscriptions-organizationid': `${zohoSubscriptionConfig.organizationId}`,
            'Authorization': `Zoho-authtoken ${zohoSubscriptionConfig.authenToken}`
          }
        }).then((response: any) => {
          resolve(response);
        })
        .catch((error: any) => {
          reject(error.message);
        });
    });
  }
}

export let zohoSubscriptionsService = new ZohoSubscriptionsService();
