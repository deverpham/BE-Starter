const jwt = require('jwt-simple');
const jwtRouter = require('express-jwt');
import {appConfig} from '../app.config';
import {userController} from '../../controller/user.controller';
const JWT_SECRET = appConfig.jwtSecret;
class JwtService {
  pack(payload: object) {
    return jwt.encode(payload, JWT_SECRET)
  }
  unpack(payload: string) {
    return jwt.decode(payload, JWT_SECRET);
  }
  checkUser() {
      return jwtRouter({
        secret: JWT_SECRET,
        isRevoked: this.saveUser
      })
  }
  saveUser(req, user, done) {
    const {email, password} = user;
    userController.findUser({
      email,
      password
    })
      .then((users: any[]) => {
        if (users.length <= 0) done(new Error('not find this user'))
        else {
          req.user = users[0];
          done()
        }
      })
      .catch(err => done(err))
  }
}
export let jwtService = new JwtService();
