const now = require('performance-now');
const eventEmitter = require('events');
export let pmTest = fn => {
  const startTime = now();
  const caculate = (timestart, comment?) => {
  const endTime = now();
  console.log((comment ? comment : '') + (endTime - timestart).toFixed(3) + "ms");
};
  fn(caculate, startTime);
};
