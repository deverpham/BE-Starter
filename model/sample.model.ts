import {ModelService} from '../core/service/model.service';
class SampleModel extends ModelService {
  constructor() {
    super()
    this.init();
  }
  async init() {
    this.currentModel = await this.connectModel('sample');
  }
}
export let sampleModel = new SampleModel();
