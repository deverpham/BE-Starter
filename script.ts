import {pmTest} from './core/service/performance.service';
import {zohoSubscriptionsService} from './core/service/zoho.subscriptions.service';
import {zohoSubscriptionConfig} from './core/app.config';
import {emailService} from './core/service/email.service';

const request = require('request');

const F: any = async (end: any, startTime: any) => {
  const result: any = await new Promise((resolve: any, reject: any) => {
    const url: any = `${zohoSubscriptionConfig.endpoint}/subscriptions`;
    const token: any = zohoSubscriptionConfig.authenToken;
    const organizationId: any = zohoSubscriptionConfig.organizationId;
    request.get({
      url,
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        'X-com-zoho-subscriptions-organizationid': organizationId,
        'Authorization': token
    }}, (err: any, response: any, body: any) => {
      if (err) {
        console.log(err);
        return resolve({status: 'error', error: 'Error while connecting to Zoho'});
      }
      if (response.statusCode === 200) {
        try {
          body = JSON.parse(body);
          resolve({status: 'success', subscriptions: body.subscriptions});
        } catch (e) {
          console.log(e);
          resolve({status: 'error', error: 'Could not parse Zoho response', body});
        }
      } else {
        console.log(body);
        try {
          body = JSON.parse(body);
          resolve({status: 'error', error: body.message});
        } catch (e) {
          console.log(e);
          resolve({status: 'error', error: 'Could not parse Zoho response', body});
        }
      }
    });
  })
  if (result.status !== 'success') {
    return console.log('run script failed');
  }
  const now: any = (new Date()).getTime();
  const expiredUsers: any = {};
  const warningUsers: any = {};
  const warningDuration: any = 10 * 86400 * 1000; // 10 days
  const subscriptions: any = result.subscriptions.reduce((preObj, curElement) => {
    preObj[curElement.subscription_id] = curElement;
    if ((curElement.next_billing_at) && (((new Date(curElement.next_billing_at)).getTime()) - now < 0)) {
      if (curElement.customer_id in expiredUsers) {
        expiredUsers[curElement.customer_id].expiredSubscriptions.push({
          subscriptionId: curElement.subscription_id,
          planName: curElement.plan_name
        });
      } else {
        expiredUsers[curElement.customer_id] = {
          user: {
            userName: curElement.customer_name,
            userEmail: curElement.email
          },
          expiredSubscriptions: [{
            subscriptionId: curElement.subscription_id,
            planName: curElement.plan_name
          }]
        }
      }
    }
    if ((curElement.next_billing_at) && (((new Date(curElement.next_billing_at)).getTime()) - now < warningDuration)) {
      if (curElement.customer_id in warningUsers) {
        warningUsers[curElement.customer_id].warningSubscriptions.push({
          subscriptionId: curElement.subscription_id,
          planName: curElement.plan_name
        });
      } else {
        warningUsers[curElement.customer_id] = {
          user: {
            userName: curElement.customer_name,
            userEmail: curElement.email
          },
          warningSubscriptions: [{
            subscriptionId: curElement.subscription_id,
            planName: curElement.plan_name,
            expiredAt: curElement.next_billing_at
          }]
        }
      }
    }
    return preObj
  }, {})

  if (Object.keys(expiredUsers).length > 0) {
    // Send list of expired Users to Admin
    const cellStyle: any = 'border-width:1px;border-style:solid;border-color:rgb(0,0,0)';
    let htmlEmail: any =
    `<table cellspacing="0" border="0">
      <tr>
        <th style='${cellStyle}'><b>User</b></th>
        <th style='${cellStyle}'><b>Email</b></th>
        <th style='${cellStyle}'><b>Expired Subscriptions</b></th>
      </tr>\n`;
    for (const userId of Object.keys(expiredUsers)) {
      htmlEmail += '<tr>';
      htmlEmail +=
        `<td style='${cellStyle}'>${expiredUsers[userId].user.userName}</td>
        <td style='${cellStyle}'>${expiredUsers[userId].user.userEmail}</td>
        <td style='${cellStyle}'>`;
      expiredUsers[userId].expiredSubscriptions.map((es: any, idx: any) => {
        htmlEmail += es.planName;
        if (idx < expiredUsers[userId].expiredSubscriptions.length - 1) {
          htmlEmail += ', ';
        }
      })
      htmlEmail += '</td></tr>\n';
    }
    htmlEmail += '</table>';
    emailService.sendNotifyEmailToAdmin(htmlEmail, 'List expired users');
    end(startTime, 'Time send email to admin: ');
  } else {
    console.log('No expired user today.');
  }

  if (Object.keys(warningUsers).length > 0) {
    // Alert to Users
    for (const userId of Object.keys(warningUsers)) {
      let htmlEmail: any = 'Some of your subscriptions will expire soon.<br/>';
      warningUsers[userId].warningSubscriptions.map((ws: any, idx: any) => {
        htmlEmail += `Subscription ${ws.planName} will expire at: ${ws.expiredAt}<br/>`;
      })
      emailService.sendMailTemplate(warningUsers[userId].user.userEmail,
         htmlEmail, 'Your Subscriptions will expire soon');
    }
    end(startTime, `Time send emails to ${Object.keys(warningUsers).length} users: `);
  } else {
    console.log('No warning user today.');
  }
  console.log('done daily script');
}

export let f = F;
