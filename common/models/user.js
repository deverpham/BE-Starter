'use strict';

module.exports = function(User) {
  User.validatesUniquenessOf('email', {message: 'email already exists'});
};
