import {pmTest} from './core/service/performance.service';
require('dotenv').config();
const loopback = require('loopback');
const express = require('express');
const boot = require('loopback-boot');
const cors  = require('cors');
const bodyParser = require('body-parser');
export let app = loopback();
pmTest((end, startTime) => {
const path = require('path');
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(cors({
  exposedHeaders: ['Accept-Ranges', 'Content-Encoding', 'Content-Length']
}));
//Use router here
boot(app, path.join(__dirname, '../server'), () => {
  const PORT = process.env.PORT; // Best Practice
  app.listen(PORT, () => {
    console.log(`Server started \r\nlisten ${PORT || 4002}`);
    end(startTime, "Time Boostrap APP: ");
  });
});
});
